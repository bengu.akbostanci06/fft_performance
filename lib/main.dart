import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:fftea/fftea.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  Future<String> loadAsset() async {
    return await rootBundle.loadString('assets/TestInputForMobile.txt');
  }

  void testFFT() async {
    final stopwatch = Stopwatch()..start();
// doSomething();

    print('Here');
    String readStr = await loadAsset();
    // var myFile = File('assets/TestInputForMobile.txt');

    // var read1 = myFile.readAsLinesSync();
    var read1 = readStr.split('\n');
    // print(read1);
    var rowNum = read1.length;
    var columnNum = 1024;
    // var inputList = doubleList(read1, rowNum, columnNum);

// ****** FFT PART ******
    // List<List<double>> fftOutReal = [];
    // List<List<double>> fftOutImag = [];
    List<Float64x2List> lst = [];

    for (int rowIndex = 0; rowIndex < rowNum; rowIndex++) {
      var rowData = read1[rowIndex].split(' ');
      rowData.removeWhere((item) => ['', null, false, 0].contains(item));
      List<double> doubleVal = [];
      for (int columnIndex = 0; columnIndex < columnNum; columnIndex++) {
        doubleVal.add(double.parse(rowData[columnIndex].toString()));
      }
      final stopwatch2 = Stopwatch()..start();
      var fft_calculator = FFT(columnNum);
      var spectrum_data = fft_calculator.realFft(doubleVal);
      // print("fun******start");
      // print('doSomething() executed2 in ${stopwatch2.elapsedMilliseconds}');
      // print('doSomething() executed2 in ${stopwatch2.elapsed}');
      // print('doSomething() executed2 in ${stopwatch2.elapsedTicks}');
      // print("fun************finish");
      lst.add(spectrum_data);

      // print(spectrum_data);
      // List<double> fftOutRealRow = [];
      // List<double> fftOutImagRow = [];
      // for (int i = 0; i < columnNum; i++) {
      //   fftOutRealRow.add(double.parse(spectrum_data[i].x.toString()));
      //   fftOutImagRow.add(double.parse(spectrum_data[i].y.toString()));

      //   // print(spectrum_data[i]);
      // }
      // fftOutReal.add(fftOutRealRow);
      // fftOutImag.add(fftOutImagRow);
    }
    // print(fftOutReal);
    // print('\n *********************** \n');
    // print(fftOutImag);

    // String fftOutRealFile = 'TestOutputFFTReal.txt';
    // String fftOutImagFile = 'TestOutputFFTImag.txt';
    // List<List<double>> expectedFftOutReal =
    //     readFileAsDoubleList(fftOutRealFile);
    // List<List<double>> expectedFftOutImag =
    //     readFileAsDoubleList(fftOutImagFile);

    // List<double> realdiff = findDiff(fftOutReal, expectedFftOutReal);
    // print(realdiff);
    // print('\n *********************** \n');
    // List<double> imagdiff = findDiff(fftOutImag, expectedFftOutImag);
    // print(imagdiff);
    print('doSomething() executed in ${stopwatch.elapsedMilliseconds}');
    print('doSomething() executed in ${stopwatch.elapsed}');
    print('doSomething() executed in ${stopwatch.elapsedTicks}');
  }

// ignore: lines_longer_than_80_chars
  List<double> findDiff(
      List<List<double>> foundList, List<List<double>> expectedList) {
    List<double> error = [];
    var rowNum = foundList.length;
    var columnNum = foundList[0].length;
    for (int rowIndex = 0; rowIndex < rowNum; rowIndex++) {
      // List<double> diff = [];
      double diff = 0;
      for (int columnIndex = 0; columnIndex < columnNum; columnIndex++) {
        double d = (expectedList[rowIndex][columnIndex] -
                foundList[rowIndex][columnIndex])
            .abs();
        diff += d;
      }
      error.add(diff);
    }

    return error;
  }

  List<List<double>> readFileAsDoubleList(String filePath) {
    // read File
    var myFile = File(filePath);
    var readLines = myFile.readAsLinesSync();

    var rowNum = readLines.length;
    var columnNum = 1024;

    List<List<double>> sampleDouble = [];

    for (int rowIndex = 0; rowIndex < rowNum; rowIndex++) {
      var rowData = readLines[rowIndex].split(' ');
      rowData.removeWhere((item) => ['', null, false, 0].contains(item));
      List<double> doubleVal = [];
      for (int columnIndex = 0; columnIndex < columnNum; columnIndex++) {
        doubleVal.add(double.parse(rowData[columnIndex].toString()));
      }
      sampleDouble.add(doubleVal);
    }

    return sampleDouble;
  }

  @override
  Widget build(BuildContext context) {
    testFFT();
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            ElevatedButton(
                onPressed: () {
                  testFFT();
                },
                child: const Text('Get FFT')),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
